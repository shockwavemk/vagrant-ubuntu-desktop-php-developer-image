# Basic Magento Developer Image

This project contains the Vagrant and provision files for a Magento Developer Image Template.

Pre-Installed software:

- git 
- mysql-workbench 
- phpstorm 
- giteye 
- composer
- docker

## Installation

1. Clone current repository: `git@bitbucket.org:shockwavemk/vagrant-ubuntu-developer.git`  

2. Replace ssh keys: `id_rsa` and `id_rsa.pub` in project folder with your bitbucket / github keys

3. Modify `provision.sh` in project folder

4. Download and start VM by using Vagrantfile:  
    - open a console
    - cd /path/to/this/project
	- vagrant up

## Folder structure

### project root

This folder contains virtual maschine upgrading stuff only e.g.:  

- Git specific files  
- Vagrant file  
- provision.sh

## Roadmap and Ideas

- TBA

## Architectural decisions  

- TBA

---
author: Martin Kramer  
created: 2014-10-17  
updated: 2014-10-17  
tag: Vagrant, Ubuntu, Developer, Template 