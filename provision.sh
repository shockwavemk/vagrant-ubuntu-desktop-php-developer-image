#!/bin/bash

# Docker Key
apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys 36A1D7869245C8950F966E92D8576A8BA88D21E9
sh -c "echo deb https://get.docker.io/ubuntu docker main > /etc/apt/sources.list.d/docker.list"

apt-get update -qy
apt-get upgrade -qy

# Install deployment-key
mkdir -p ~/.ssh

cat >~/.ssh/config <<EOL
Host *
StrictHostKeyChecking no
EOL

cp -r /vagrant/id_rsa ~/.ssh/id_rsa
cp -r /vagrant/id_rsa.pub ~/.ssh/id_rsa.pub

chmod 700 ~/.ssh
chmod 600 ~/.ssh/id_rsa